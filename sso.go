package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"os"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/sso"
	"github.com/aws/aws-sdk-go-v2/service/ssooidc"
	"github.com/aws/aws-sdk-go-v2/service/sts"

	"github.com/pkg/browser"
)

const (
	fetchOIDCGrant = "urn:ietf:params:oauth:grant-type:device_code" // grant type
	// ssoClientName is what this app identifies towards authentication services as;
	// choosing different values doesn't seem to affect the login flow
	ssoClientName = "HelloSSO"
)

// showSSOProfile shows an AWS SSO profile via `aws configure sso`.
func showSSOProfile(configProfile string) {
	cfg, err := config.LoadDefaultConfig(
		context.TODO(),
		config.WithSharedConfigProfile(configProfile),
	)
	if err != nil {
		fmt.Println("error:", err)
		os.Exit(1)
	}

	client := sts.NewFromConfig(cfg)

	identity, err := client.GetCallerIdentity(
		context.TODO(),
		&sts.GetCallerIdentityInput{},
	)
	if err != nil {
		fmt.Println("error:", err)
		os.Exit(1)
	}

	fmt.Printf(
		"Account: %s\nUserID: %s\nARN: %s\n",
		aws.ToString(identity.Account),
		aws.ToString(identity.UserId),
		aws.ToString(identity.Arn),
	)
}

func main() {
	var (
		startURL      string
		configProfile string
		awsRegion     string
	)

	flag.StringVar(&startURL, "start-url", "", "AWS SSO Start URL")
	flag.StringVar(&configProfile, "config-profile", "", "AWS SSO config profile from 'aws sso configure'")
	flag.StringVar(&awsRegion, "aws-region", "", "AWS default region")
	flag.Parse()
	if startURL == "" && configProfile == "" {
		fmt.Printf("Either set -start-url and -aws-region, or log in with AWS cli using 'aws sso configure' and set -config-profile accordingly.")
		flag.Usage()
		os.Exit(1)
	}
	if configProfile != "" {
		showSSOProfile(configProfile)
		os.Exit(1)
	}
	// load default aws config (from ~/.aws/config, environment variables, IAM roles of ECS/EC2 instances..),
	// we make no assumptions that any configs are available here, but respect ones that do exist so -aws-region
	// flag doesn't have to be set explicitly
	cfg, err := config.LoadDefaultConfig(context.TODO())
	if err != nil {
		fmt.Printf("failed to load default AWS config: %v\n", err)
		os.Exit(1)
	}
	if cfg.Region == "" {
		if awsRegion != "" {
			cfg.Region = awsRegion
		} else {
			fmt.Printf("no region specified in config and no -aws-region set: %+v\n", cfg)
			os.Exit(1)
		}
	}
	// create sso oidc oidcClient to trigger login flow
	oidcClient := ssooidc.NewFromConfig(cfg)
	if err != nil {
		fmt.Printf("failed to create SSO IDC client: %v", err)
		os.Exit(1)
	}
	// register ourselves as a client triggering the login flow; output should be persisted here
	// across many authentication requests
	register, err := oidcClient.RegisterClient(context.TODO(), &ssooidc.RegisterClientInput{
		ClientName: aws.String(ssoClientName),
		ClientType: aws.String("public"),
		Scopes:     []string{"sso-portal:*"},
	})
	if err != nil {
		fmt.Printf("failed to register SSO IDC client: %v\n", err)
		os.Exit(1)
	}
	// authorize user's device using the client registration response
	auth, err := oidcClient.StartDeviceAuthorization(context.TODO(), &ssooidc.StartDeviceAuthorizationInput{
		ClientId:     register.ClientId,
		ClientSecret: register.ClientSecret,
		StartUrl:     aws.String(startURL),
	})
	if err != nil {
		fmt.Printf("failed to start device authorization flow: %v\n", err)
		os.Exit(1)
	}
	// trigger OIDC login; open browser to login, close tab once login is done, press enter to continue
	url := aws.ToString(auth.VerificationUriComplete)
	fmt.Printf("If browser is not opened automatically, please open link:\n%v\n", url)
	err = browser.OpenURL(url)
	if err != nil {
		fmt.Printf("failed to open URL in browser: %v\n", err)
		os.Exit(1)
	}
	fmt.Println("Press ENTER key once login is done")
	_ = bufio.NewScanner(os.Stdin).Scan()
	// generate sso access token, which can fetch short-lived credentials for assigned roles in the AWS account
	token, err := oidcClient.CreateToken(context.TODO(), &ssooidc.CreateTokenInput{
		ClientId:     register.ClientId,
		ClientSecret: register.ClientSecret,
		DeviceCode:   auth.DeviceCode,
		GrantType:    aws.String(fetchOIDCGrant),
	})
	if err != nil {
		fmt.Printf("failed to create OIDC token: %v\n", err)
		os.Exit(1)
	}
	// create sso client
	ssoClient := sso.NewFromConfig(cfg)
	// list accounts [ONLY provided for better example coverage]
	fmt.Println("Fetching list of all accounts for user")
	accounts := sso.NewListAccountsPaginator(
		ssoClient,
		&sso.ListAccountsInput{
			AccessToken: token.AccessToken,
		},
	)
	var (
		accountID = ""
		roleName  = ""
	)
	for accounts.HasMorePages() {
		page, err := accounts.NextPage(context.TODO())
		if err != nil {
			fmt.Printf("failed to retrieve next page of accounts: %v", err)
			os.Exit(1)
		}
		for _, account := range page.AccountList {
			fmt.Println("-------------------------------------------------------")
			fmt.Printf("\nAccount ID: %v, Name: %v, Email: %v\n", aws.ToString(account.AccountId), aws.ToString(account.AccountName), aws.ToString(account.EmailAddress))

			// list roles for a given account [ONLY provided for better example coverage]
			fmt.Printf("\n\nFetching roles of account %v for user:\n\n", aws.ToString(account.AccountId))
			roles := sso.NewListAccountRolesPaginator(
				ssoClient,
				&sso.ListAccountRolesInput{
					AccessToken: token.AccessToken,
					AccountId:   account.AccountId,
				},
			)
			for roles.HasMorePages() {
				rs, err := roles.NextPage(context.TODO())
				if err != nil {
					fmt.Printf("failed to retrieve next page of roles: %v\n", err)
					os.Exit(1)
				}
				for _, role := range rs.RoleList {
					fmt.Printf("Account ID: %v, Role Name: %v\n", aws.ToString(role.AccountId), aws.ToString(role.RoleName))
					// we arbitrarily take the last AWS role as the one to authenticate as here
					roleName = aws.ToString(role.RoleName)
				}
			}
			// we arbitrarily take the last AWS account as the one to authenticate with here
			accountID = aws.ToString(account.AccountId)
		}
	}
	fmt.Println("-------------------------------------------------------")
	// exchange token received during oidc flow to fetch actual aws access keys
	fmt.Printf("\n\nFetching short-term credentials for role %v in account %v for user..\n", roleName, accountID)
	credentials, err := ssoClient.GetRoleCredentials(
		context.TODO(),
		&sso.GetRoleCredentialsInput{
			AccessToken: token.AccessToken,
			AccountId:   aws.String(accountID),
			RoleName:    aws.String(roleName),
		},
	)
	if err != nil {
		fmt.Printf("failed to get STS short-term credentials for role: %v\n", err)
		os.Exit(1)
	}
	// printing access key to show how they are accessed
	fmt.Printf("\n\nTemporary AWS access keys:\n")
	fmt.Println("Access key id: ", aws.ToString(credentials.RoleCredentials.AccessKeyId))
	fmt.Println("Secret access key: ", aws.ToString(credentials.RoleCredentials.SecretAccessKey))
	fmt.Println("Expiration: ", aws.ToInt64(&credentials.RoleCredentials.Expiration))
	fmt.Println("Session token: ", aws.ToString(credentials.RoleCredentials.SessionToken))
	fmt.Printf("\n\n")
}
