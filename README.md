# sso

- optional: use `aws configure sso` to configure profiles for the AWS accounts you'll use to see the flow in action
- run `SSO_START_URL=https://yourpage.awsapps.com/start make` for a demo, which will open your browser and authenticate
  with specified AWS SSO start URL