SSO_START_URL ?= https://johnstonetech.awsapps.com/start
AWS_DEFAULT_REGION ?= eu-west-1

run:
	go run sso.go -start-url $(SSO_START_URL) -aws-region $(AWS_DEFAULT_REGION)